# Fair SPDZ
## General
This repository is aimed at benchmarking the performance of the `MobileNet V1 0.25_128` implementation as created in https://github.com/data61/MP-SPDZ.git. It's results are published in the for now unpublished paper called "Extending the Security of SPDZ with Fairness".


## Dependencies
To run the example you need to have the following installed
* Python3 (Tested on Python3.8)
* Docker
* Docker-compose


### Run
To run the example use the following command
```console
$ docker-compose up --build --abort-on-container-exit
```
### Run 3 player example (locally)
To run the 3 player example use the following command
```console
$ docker-compose -f 3players.yml up --build --abort-on-container-exit
```


### Acknowledgements
This paper is based on the Master Thesis “Making SPDZ Fair” by Bart Veldhuizen (2020) as a requirement for the master Computing Science at the Radboud University, Nijmegen, NL. All research related to this thesis was performed at the Cyber Security and Robustness department, TNO, The Hague, NL. This work was further supported by the TNO Early Research Programme “Next Generation Cryptography”. The work of L. Kohl was carried out in the CWI Cryptology group, Amsterdam, supported by the NWO Talent Programme Veni (VI.Veni.222.348) and the NWO Gravitation Project QSC.
